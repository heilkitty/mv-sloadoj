# EN

* Have an account on Gitlab.

* [Create an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue),
if you notice a mistake or a problem.

* For a small contribution in a single file, you could just edit a file
in the web interface. All the necessary "formalities" (creating a branch
and a merge request) will be handled by Gitlab automatically.

* For a bigger contribution, fork the project and make a merge request.
If you're new to this git thing, there are comprehensive
[Gitlab guides](https://docs.gitlab.com/ee/gitlab-basics/) to help you out.

* Make sure your piece of translation complies with
[guidelines](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/GUIDELINES.md)
and [glossary](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/GLOSSARY.md).
If you do not agree with some of it, it could be discussed, they're not
written on stone, but a bunch of 0s and 1s.

* Mark the readiness of the translated records with a `<ready>` tag. E.g.:

     ```xml
	 <record>
		<_id>CELL</_id>
		<key>Abaelun Mine</key>
		<val>Abaelun Mine</val>
	 </record>
	 ```

	turns into

     ```xml
	 <record>
		<_id>CELL</_id>
		<key>Abaelun Mine</key>
		<val>Minejo Abaelun</val>
		<ready>1</ready>
	 </record>
	 ```

	`1` means ready, `0` not ready (in this case, the tags is not required),
	`-1` doubtful or to be revised.

* Write in English or Esperanto in discussions/comments/commit messages.

# EO

* Havu konton en Gitlabo.

* [Faru atentindaĵon](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue),
se vi rimarkas eraron aŭ problemon.

* Por malgranda kontribuaĵo en solan dorieron, vi povus redakti tiun dosieron
en la retinterfaco. Ĉiuj necesaj "formalaĵoj" (kreado de la branĉigo
kaj la kunfandpeto) estos aŭtomate traktataj per Gitlabo.

* Por pli granda kontribuaĵo, forku la projekton kaj faru kunfandpeton.
Se vi estas komencanto en tiu afero de gito, estas multampleksaj
[Gitlabaj gvidoj](https://docs.gitlab.com/ee/gitlab-basics/) por helpi vin.

* Certigu, ke via tradukaĵo konformas kun
[gvidnormoj](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/GUIDELINES.md)
kaj [glosario](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/GLOSSARY.md).
Se vi ne konsentas kun certaj punktoj, la situacio estas diskutebla, ili ne estas
skribitaj sur ŝtono, sed estas fasko de 0j kaj 1j.

* Indiku la pretecon de la tradukitaj rikordoj per `<ready>` etikedo. Ekz.:

     ```xml
	 <record>
		<_id>CELL</_id>
		<key>Abaelun Mine</key>
		<val>Abaelun Mine</val>
	 </record>
	 ```

	turniĝas en

     ```xml
	 <record>
		<_id>CELL</_id>
		<key>Abaelun Mine</key>
		<val>Minejo Abaelun</val>
		<ready>1</ready>
	 </record>
	 ```

	`1` signifas "preta", `0` "malpreta" (en ĉi tiu okazo, la etikedo estas malnecesa),
	`-1` "dubinda" aŭ "por revizio".

* Skribu viajn diskutaĵojn/komentariojn/enmetmesaĝojn en la angla aŭ Esperanto.