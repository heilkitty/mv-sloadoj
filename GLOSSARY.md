**EN** A glossary for "Morrovindo por Sloadoj" project.

**EO** Glosaro por "Morrovindo por Sloadoj" projekto.

# People

## Nations

EN | EO | notes
---|----|----------------
Altmer | Altmer-
Argonian | Argonian-
Ashlander | Cindrul-
Bosmer | Bosmer-
Breton | Breton-
Cyrodiil | Kirodiul-
Dark Elf | Malhela Elfo / Malhelelf-
Dunmer | Danmer-
Dwarf | Nano
Dwemer | Dvemer-
High Elf | Alta Elfo / Altelf-
Imperial | Imperiul-
Khajiit | Kaĝit-
Nord | Nordan-
Orc | Ork-
Orsimer | Orsimer-
Velothi | Velotul-
Wood Elf | Arba Elfo / Arbelf-

## VIPs

EN | EO | notes
---|----|----------------
Almalexia | Almaleksio
Barenziah | Barenzia
Dagoth Ur | Dagoto Ur
Helseth | Helset
Nerevar | Nerevaro
Nerevarine | Nerevarulo | lit. "Nerevar-person"
Sotha Sil | Sota Silo
Veloth | Veloto
Vivec | Viveko

## Other

EN | EO | notes
---|----|----------------
Crazy-Legs Arantamo | Frenezaj-Kruroj Arantamo
Fast Eddie | Vigla Eĉjo
Gentleman Jim Stacey | Ĝentlemano Jim Stacey
Lustidrike | Voluptvantaĵo

# Places

## Countries, landmasses, realms, etc

EN | EO | notes
---|----|----------------
Akavir | Akaviro
Argonia | Argonio
Atmora | Atmoro
Ashlands | Cindrolandoj
Black Marsh | Nigra Marĉo
Cyrodiil | Kirodilo
Elsweyr | Ioliloko
Hammerfell | Martelfelo
High Rock | Alta Roko
Morrowind | Morrovindo
Nirn | Nirno
Skyrim | Skajrimo
Summerset | Somersubiro
Summerset Isle | Somersubira Insulo
Tamriel | Tamrielo
Vvardenfell | Vvardenfelo

## Cities, towns, villages, etc

EN | EO | notes
---|----|----------------
Arkngthunch-Sturdumz | Arkntunĉ-Ŝturdumc
Ebonheart | Ebonkoro
Ghostgate | Spiritpordego
Moonmoth Legion Fort | Fuorto de Lunapapilia Legio
Mzuleft | Mculeft
Nchurdamz | Nĉurdamc
Wolverine Hall | Halo de Gulo

## Other

EN | EO | notes
---|----|----------------
egg mine | ovominejo | e.g. Asha-Ahhe Egg Mine → Ovominejo Aŝa-Aĥe
mine | minejo | e.g. Dissapla Mine → Minejo Dissapla

# Magic

EN | EO | notes
---|----|----------------
bound | bind- | Bound Dagger → Binda Ponardo / Bindi ponardon
summon / call | alvoki

# Warfare

EN | EO | notes
---|----|----------------
battlemage | batalmago
dai-katana | dajkatano
katana | katano

# Politics

EN | EO | notes
---|----|----------------
ashkhan | cindĥano
great house | granda domo | e.g. [Great] House Telvanni → [Granda] Domo de Telvanni
gulakhan | gulaĥano
hetman | hetmano
hortator | hortatoro
Tribunal | Tribunalo

# Religion

EN | EO | notes
---|----|----------------
Imperial Cult | Imperia Kulto
Lady (as in Lady Almalexia or Lady Azura) | Senjorino
Lord (as in Lord Vivec or Lord Dagoth) | Senjoro | The idea is that "Lord" is a religious title in this case, thus using "Senjoro" instead of "lordo".
Nerevarine Cult | Kulto de Nerevarulo / Nerevarula Kulto
Tribunal Temple | Templo de Tribunalo / Tribunala Templo

# Health

EN | EO | notes
---|----|----------------
Blight | Pesto
blight disease | pesta malsano
common disease | ordinara malsano
Corprus | Korpruso / Corprus
Yellow Tick | Flava Akaro

# Other

EN | EO | notes
---|----|----------------
ebony | ebon-
glass (as in material for armor and weapons) | vitr-
Violet Coprinus | Viola Sterkofungo