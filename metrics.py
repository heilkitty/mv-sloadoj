#!/usr/bin/env python3

import os
from pathlib import Path
import re
import sys

if __name__ == '__main__':
    ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

    total_records = 0
    ready_records = 0
    fuzzy_records = 0

    # iterating over all *.xml files in the repo dir, case insensitive
    for filename in Path(ROOT_DIR).rglob('*.xml'):
        with open(filename, 'rt') as f:
            # Techically, our XML file is malformed AF, so we use "manual" parsing
            # (we don't wanna use lxml or beautiful soup, and python's standard 
            # library parser won't cut it). This approach does require, however,
            # that each <record> or </record> tag is in the separate line.
            record_content = "" # inside contents of <record> tag
            state = 0 # 0: we're "outside" <record></record>, 1: "inside"
            cur_line = 0 # line number, for error output
            for line in f:
                cur_line += 1
                _line = line.strip()
                if state == 0:
                    if _line == "":
                        continue
                    elif _line == "<record>":
                        # moving "inside" the <record>
                        state = 1
                        record_content = ""
                        total_records += 1
                        continue
                    else:
                        sys.stderr.write("Error: {0} is too damn malformed, "
                                         "invalid root tag, line {1}.\n".format(filename, cur_line))
                        sys.exit(1)
                elif state == 1:
                    if _line == "":
                        continue
                    elif _line == "<record>":
                        sys.stderr.write("Error: {0} is too damn malformed, "
                                         "multiple <record> tags, line {1}.\n".format(filename, cur_line))
                        sys.exit(1)
                    elif _line == "</record>":
                        # moving "outside" the <record>
                        state = 0
                        # finding all <ready> tags
                        _ready_matches = re.findall(r'<ready>(.?)</ready>', record_content,
                                                    re.DOTALL or re.MULTILINE or re.UNICODE)
                        if len(_ready_matches) > 1:
                            sys.stderr.write("Error: {0} is too damn malformed, "
                                             "multiple <ready> tags, line {1}.\n".format(filename, cur_line))
                            sys.exit(1)
                        elif len(_ready_matches) == 1:
                            try:
                                readiness_state = int(_ready_matches[0])
                            except ValueError:
                                readiness_state = 0
                            if readiness_state == 1: # ready
                                ready_records += 1
                            elif readiness_state == -1: # doubtful or to be revised
                                fuzzy_records += 1 
                            else:
                                pass
                            continue
                        else:
                            continue
                    else:
                        record_content = record_content + _line + "\n"
                        continue

    print("total_records", total_records)
    print("ready_records", ready_records)
    print("ready_records_percent", "{0:.1f}".format(ready_records / total_records * 100))
    print("fuzzy_records", fuzzy_records)
    print("fuzzy_records_percent", "{0:.1f}".format(fuzzy_records / total_records * 100))
