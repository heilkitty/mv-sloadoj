Here are some major guidelines for the project, in no particular order.
These are not absolute, we may modify them and even have exceptions.
For exceptions and special cases, see
[glossary](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/GLOSSARY.md).

# Transcription

letters | transcription | examples | notes
--------|---------------|----------|-------
a | a
c | k
ch | ĥ/ĉ | Achel → Aĥel / Chun-Ook → Ĉun-Ūk | also, "ŝ", if the word looks suspiciously French 
w | v/ŭ | Morrovindo, Dvemeroj / An-Zaw → An-Zaŭ | "ŭ" is, probably, suitable only at the end of the word
th | t | Dagoto | maybe, "f" or "z", if you feel like it's more appropriate
qu | kv/kŭ | Olquar → Olkvar
q | k | Qorwynn → Korvinn
ae | e/ē (prolongated e) | Nibani Mēsa | as in IPA's [æ]
x | ks | Nelix → Neliks | also, "s"/"ŝ", e.g., in Akaviri words (if there are any in Morrowind)
j | ĝ | J'Rasha → Ĝ'Raŝa | also, "j", if it's more appropriate, esp. in Nordic words
y | i/j | Aryon → Arion, Rararyn Radarys → Rararin Radaris / Yokuda → Jokudo | maybe, "ju" in Nordic words
ou | u | Hassour → Hassur
h | h/ĥ
gh | g
oo | u/ū (prolongated u)
kh | k
ts | c
zh | ĵ
sh | ŝ
hn | nn

## Dwemeri  words

The "look and feel" of Dwemeris seems to be inspired by German,
so, some letters and digraphs should be transcribed,
like it was a transcription of a German word.

Examples: Nchurdamz → Nĉurdamc, Arkngthunch-Sturdumz → Arkntunĉ-Ŝturdumc, Mzuleft → Mculeft.

## Daedric font

Text, which is supposed to be written with Daedric letters,
should be written using X-sistemo, if there are any Esperanto-specific letters in it.

# Names

Names are transcribed (1), names of deities, ancient heroes and other VIPs
may (or may not, see [Bibliaj personoj](https://eo.wikipedia.org/wiki/Kategorio:Bibliaj_personoj))
be also "esperantized" (2). Unless the name is taken from
Latin/French/English/something-something-Norse language. In that case
it should be left alone (3). Nicknames should be translated (4)
(or even some names if you strongly feel, that etymology of the name is important (5)).
Examples:

1. Ughash gro-Batul → Ugaŝ gro-Batul, Achel → Aĥel, Alavesa Samori → Alavesa Samori,
   Effe-Tei → Effe-Tej, Favise Selaren → Favise Selaren, J'Rasha → Ĝ'Raŝa,
   Beyte Fyr → Bejte Fir, Hassour Zainsubani → Hassur Zainsubani,
   Savile Imayn → Savil Imajn, Nibani Maesa → Nibani Mēsa, 
   Radac Stungnthumz → Radak Ŝtungntumc.

1. Helseth → Helset, Vivec → Viveko, Almalexia → Almaleksio, Dagoth Ur → Dagoto Ur.

1. Crassius Curio → Crassius Curio, Dabienne Mornardl → Dabienne Mornardl,
   Jon Hawker → Jon Hawker, Fryssa → Fryssa.

1. Crazy-Legs Arantamo → Frenezaj-Kruroj Arantamo, Widow Vabdas → Vidvino Vabdas,
   Fast Eddie → Vigla Eĉjo, Anja Swift-Sailer → Anja Rapida-Velŝipo,
   Gentleman Jim Stacey → Ĝentlemano Jim Stacey, Snedbrir the Smith → Snedbrir la Forĝisto.

1. Nerevarine → Nerevarulo, Lustidrike → Voluptvantaĵo, ~~Hyarnarenquar → 23478682352~~.

# Toponyms

As a general rule, toponyms of large landmasses, such as continents and countries,
are transcribed and "esperantized". E.g., Tamrielo, Morrovindo, Skajrimo.
When it makes sense, translated, e.g., Nigra Marĉo (but: Argonio).

Toponyms of lesser entities, such as cities, are transcribed (1), unless they are
definitely Latin/French/English/something-something-Norse (2) (as is with names).
When it makes sense, translated (3). Examples:

1. Tel Fyr → Tel Fir, Urshilaku Camp → Tendaro Urŝilaku, Ashalmawia → Aŝalmavia,
   Sadrith Mora → Sadrit Mora, Tel Aruhn → Tel Arunn.

1. Pelagiad, Caldera, Sjobal, Frykte.

1. Ebonheart → Ebonkoro, Ghostgate → Spiritpordego, Wolverine Hall → Halo de Gulo,
   Moonmoth Legion Fort → Fuorto de Lunapapilia Legio.
