# EN

**Morrovindo por Sloadoj** is a project aiming to translate
the game of "TES III: Morrowind" into Esperanto.
At this stage, only text information in `*.esm` files is worked on.
The translation is done using the [YAMPT](https://www.nexusmods.com/morrowind/mods/44518) util.
The scope for now is base game (+ 2 DLCs); in the future, official mods and
unofficial patches are to be supported. Please, read
[CONTRIBUTING.md](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/CONTRIBUTING.md),
if you want to help the project.

# EO

**Morrovindo por Sloadoj** estas projekto por traduki
la ludon de "TES III: Morrowind" en Esperanton.
Dum ĉi tiu etapo, nur teksta informacio en `*.esm` dosieroj estas laborata.
La traduko estas farata per [YAMPT](https://www.nexusmods.com/morrowind/mods/44518) utilprogramo.
La amplekso por nun estas baza ludo (+ 2 EŜEoj); en futuro, oficialaj modoj kaj
maloficialaj flikaĵoj estos subtenataj. Bonvolu legi
[CONTRIBUTING.md](https://gitlab.com/heilkitty/mv-sloadoj/blob/master/CONTRIBUTING.md),
se vi volas helpi la projekton.